import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter as Router, Route } from 'react-router-dom'

import './style.css'
import Rpsdetail from './views/rpsdetail'
import Gamelist from './views/gamelist'
import Ticdetail from './views/ticdetail'
import Landing from './views/landing'
import tictactoe from './views/ttt.jsx'
import Profile from './views/profile'
import Rps from './views/Rps'

import Login from './components/Login/login'
import Register from './components/Register/register'

const App = () => {
  return (
    <Router>
      <div>
        <Route component={Rpsdetail} exact path="/rpsdetail" />
        <Route component={Login} exact path="/login" />
        <Route component={Gamelist} exact path="/gamelist" />
        <Route component={Ticdetail} exact path="/ticdetail" />
        <Route component={Landing} exact path="/" />
        <Route component={Register} exact path="/register" />
        <Route component={tictactoe} exact path="/ttt" />
        <Route component={Profile} exact path="/profile" />
        <Route component={Rps} exact path="/Rps" />
      </div>
    </Router>
  )
}

ReactDOM.render(<App />, document.getElementById('app'))

import React from 'react'
import { Link } from 'react-router-dom'

import PropTypes from 'prop-types'

import './listcard2.css'

const Listcard2 = (props) => {
  return (
    <div className={`listcard2-testimonial-card ${props.rootClassName} `}>
      <svg viewBox="0 0 950.8571428571428 1024" className="listcard2-icon">
        <path
          d="M438.857 182.857v402.286c0 161.143-131.429 292.571-292.571 292.571h-36.571c-20 0-36.571-16.571-36.571-36.571v-73.143c0-20 16.571-36.571 36.571-36.571h36.571c80.571 0 146.286-65.714 146.286-146.286v-18.286c0-30.286-24.571-54.857-54.857-54.857h-128c-60.571 0-109.714-49.143-109.714-109.714v-219.429c0-60.571 49.143-109.714 109.714-109.714h219.429c60.571 0 109.714 49.143 109.714 109.714zM950.857 182.857v402.286c0 161.143-131.429 292.571-292.571 292.571h-36.571c-20 0-36.571-16.571-36.571-36.571v-73.143c0-20 16.571-36.571 36.571-36.571h36.571c80.571 0 146.286-65.714 146.286-146.286v-18.286c0-30.286-24.571-54.857-54.857-54.857h-128c-60.571 0-109.714-49.143-109.714-109.714v-219.429c0-60.571 49.143-109.714 109.714-109.714h219.429c60.571 0 109.714 49.143 109.714 109.714z"
          className=""
        ></path>
      </svg>
      <div className="listcard2-testimonial">
        <img
          alt={props.image_alt}
          src={props.image_src}
          className="listcard2-image"
        />
        <span className="listcard2-text">{props.tttoetitle}</span>
        <div className="listcard2-container">
          <Link to="/ticdetail" className="listcard2-navlink button">
            {props.go}
          </Link>
        </div>
      </div>
    </div>
  )
}

Listcard2.defaultProps = {
  go: 'Go!',
  tttoetitle: 'Tic Tac Toe',
  rootClassName: '',
  image_src: '/playground_assets/t3oe-500h.webp',
  image_alt: 'image',
}

Listcard2.propTypes = {
  go: PropTypes.string,
  tttoetitle: PropTypes.string,
  rootClassName: PropTypes.string,
  image_src: PropTypes.string,
  image_alt: PropTypes.string,
}

export default Listcard2

import { FirebaseError } from 'firebase/app';
import React, { useCallback } from 'react'
import { BrowserRouter, Link, Navigate, Outlet, Route, Routes, Redirect } from 'react-router-dom';
import { Helmet } from 'react-helmet'
import NavigationLinks1 from '../navigation-links1'
import { useAuth } from '../../context/auth';
import './register.css'

const Register = (props) => {
  const { signup } = useAuth();
  // sama aja kayak declare let username = ''
  // const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  // const [phoneNumber, setPhoneNumber] = useState('');
  const [password, setPassword] = useState('');
  // const [confirmPassword, setConfirmPassword] = useState('');

  // kenapa use callBack ? 
  const handleClickResigter = useCallback(async () => {
      // (?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})
      // var regexLower = new RegExp("^(?=.*[a-z])");
      // var regexUpper = new RegExp("^(?=.*[A-Z])");
      // var regexNumeric = new RegExp("^(?=.*[0-9])");
      // var regexLength = new RegExp("(?=.{8,}");
      // var regexSpecialCharacter = new RegExp("^(?=.*[!@#\$%\^&\*])");
      
      // if(!username || !email || !phoneNumber || !password || !confirmPassword) throw new Error('Please fill all column input !');
      // if(username.length < 6) throw new Error('Username minimum 6 character');
      // if(phoneNumber.length < 6) throw new Error('Phone number minimum 10 digit');
      // if(confirmPassword !== password) throw new Error('Confirm Password must be same with password !');
      // if(regexLower(password)) throw new Error('Password must contain at least 1 lowercase alphabetical character'); 
      // if(regexUpper(password)) throw new Error('Password must contain at least 1 uppercase alphabetical character'); 
      // if(regexNumeric(password)) throw new Error('Password must contain at least 1 numeric character'); 
      // if(regexSpecialCharacter(password)) throw new Error('Password must contain at least one special character'); 
      // if(regexLength(password)) throw new Error('Password must be eight characters or longer'); 
      
      // await signup(username, email, phoneNumber, password, confirmPassword); // <- kalo mau redirect ke login, maka harus signup dulu supaya terdaftar ke database
      await signup(email, password); // <- kalo mau redirect ke login, maka harus signup dulu supaya terdaftar ke database
      // redirect('/login');
      // [username,email,phoneNumber,password,confirmPassword,signup] <- udah format dari useCallback
      // semua variable ditaro didalam array callback ini
  }, [email, password,signup]) // const { signup } = useAuth();
  

  return (
    <div className="register-container">
      <Helmet>
        <title>Register - Challenge Chapter 9</title>
        <meta property="og:title" content="register - Challengech9plat" />
      </Helmet>
      <header data-role="Header" className="register-header">
        <div className="register-nav">
          <Link to="/" className="register-navlink">
            <img
              alt="image"
              src="/playground_assets/ch1-200h.png"
              className="register-image"
            />
          </Link>
        </div>
        <div className="register-btn-group">
          <Link to="/login" className="register-login button">
            Login
          </Link>
          <button className="register-register button">Register</button>
        </div>
        <div data-role="BurgerMenu" className="register-burger-menu">
          <svg viewBox="0 0 1024 1024" className="register-icon">
            <path d="M128 554.667h768c23.552 0 42.667-19.115 42.667-42.667s-19.115-42.667-42.667-42.667h-768c-23.552 0-42.667 19.115-42.667 42.667s19.115 42.667 42.667 42.667zM128 298.667h768c23.552 0 42.667-19.115 42.667-42.667s-19.115-42.667-42.667-42.667h-768c-23.552 0-42.667 19.115-42.667 42.667s19.115 42.667 42.667 42.667zM128 810.667h768c23.552 0 42.667-19.115 42.667-42.667s-19.115-42.667-42.667-42.667h-768c-23.552 0-42.667 19.115-42.667 42.667s19.115 42.667 42.667 42.667z"></path>
          </svg>
        </div>
        <div data-role="MobileMenu" className="register-mobile-menu">
          <div className="register-nav1">
            <div className="register-container1">
              <img
                alt="image"
                src="https://presentation-website-assets.teleporthq.io/logos/logo.png"
                className="register-image1"
              />
              <div data-role="CloseMobileMenu" className="register-menu-close">
                <svg viewBox="0 0 1024 1024" className="register-icon02">
                  <path d="M810 274l-238 238 238 238-60 60-238-238-238 238-60-60 238-238-238-238 60-60 238 238 238-238z"></path>
                </svg>
              </div>
            </div>
            <NavigationLinks1 rootClassName="rootClassName13"></NavigationLinks1>
          </div>
          <div>
            <svg
              viewBox="0 0 950.8571428571428 1024"
              className="register-icon04"
            >
              <path d="M925.714 233.143c-25.143 36.571-56.571 69.143-92.571 95.429 0.571 8 0.571 16 0.571 24 0 244-185.714 525.143-525.143 525.143-104.571 0-201.714-30.286-283.429-82.857 14.857 1.714 29.143 2.286 44.571 2.286 86.286 0 165.714-29.143 229.143-78.857-81.143-1.714-149.143-54.857-172.571-128 11.429 1.714 22.857 2.857 34.857 2.857 16.571 0 33.143-2.286 48.571-6.286-84.571-17.143-148-91.429-148-181.143v-2.286c24.571 13.714 53.143 22.286 83.429 23.429-49.714-33.143-82.286-89.714-82.286-153.714 0-34.286 9.143-65.714 25.143-93.143 90.857 112 227.429 185.143 380.571 193.143-2.857-13.714-4.571-28-4.571-42.286 0-101.714 82.286-184.571 184.571-184.571 53.143 0 101.143 22.286 134.857 58.286 41.714-8 81.714-23.429 117.143-44.571-13.714 42.857-42.857 78.857-81.143 101.714 37.143-4 73.143-14.286 106.286-28.571z"></path>
            </svg>
            <svg
              viewBox="0 0 877.7142857142857 1024"
              className="register-icon06"
            >
              <path d="M585.143 512c0-80.571-65.714-146.286-146.286-146.286s-146.286 65.714-146.286 146.286 65.714 146.286 146.286 146.286 146.286-65.714 146.286-146.286zM664 512c0 124.571-100.571 225.143-225.143 225.143s-225.143-100.571-225.143-225.143 100.571-225.143 225.143-225.143 225.143 100.571 225.143 225.143zM725.714 277.714c0 29.143-23.429 52.571-52.571 52.571s-52.571-23.429-52.571-52.571 23.429-52.571 52.571-52.571 52.571 23.429 52.571 52.571zM438.857 152c-64 0-201.143-5.143-258.857 17.714-20 8-34.857 17.714-50.286 33.143s-25.143 30.286-33.143 50.286c-22.857 57.714-17.714 194.857-17.714 258.857s-5.143 201.143 17.714 258.857c8 20 17.714 34.857 33.143 50.286s30.286 25.143 50.286 33.143c57.714 22.857 194.857 17.714 258.857 17.714s201.143 5.143 258.857-17.714c20-8 34.857-17.714 50.286-33.143s25.143-30.286 33.143-50.286c22.857-57.714 17.714-194.857 17.714-258.857s5.143-201.143-17.714-258.857c-8-20-17.714-34.857-33.143-50.286s-30.286-25.143-50.286-33.143c-57.714-22.857-194.857-17.714-258.857-17.714zM877.714 512c0 60.571 0.571 120.571-2.857 181.143-3.429 70.286-19.429 132.571-70.857 184s-113.714 67.429-184 70.857c-60.571 3.429-120.571 2.857-181.143 2.857s-120.571 0.571-181.143-2.857c-70.286-3.429-132.571-19.429-184-70.857s-67.429-113.714-70.857-184c-3.429-60.571-2.857-120.571-2.857-181.143s-0.571-120.571 2.857-181.143c3.429-70.286 19.429-132.571 70.857-184s113.714-67.429 184-70.857c60.571-3.429 120.571-2.857 181.143-2.857s120.571-0.571 181.143 2.857c70.286 3.429 132.571 19.429 184 70.857s67.429 113.714 70.857 184c3.429 60.571 2.857 120.571 2.857 181.143z"></path>
            </svg>
            <svg
              viewBox="0 0 602.2582857142856 1024"
              className="register-icon08"
            >
              <path d="M548 6.857v150.857h-89.714c-70.286 0-83.429 33.714-83.429 82.286v108h167.429l-22.286 169.143h-145.143v433.714h-174.857v-433.714h-145.714v-169.143h145.714v-124.571c0-144.571 88.571-223.429 217.714-223.429 61.714 0 114.857 4.571 130.286 6.857z"></path>
            </svg>
          </div>
        </div>
      </header>
      <div className="register-hero">
        <h1 className="register-text">
          {/* <span>Register your Account</span> */}
          <br></br>
        </h1>
        <div className="register-container2">
          <span className="register-text3">Username      </span>
          <input
            type="username" 
            value={username} 
            onChange={(e) => setUsername(e.target.value)}
            required
            className="input"
          />
        </div>
        <br />
        <div className="register-container3">
          <span className="register-text4">Password       </span>
          <input
            type="password" 
            value={password} 
            onChange={(e) => setPassworrd(e.target.value)}
            className="input" 
          />
        </div>
        <br />
        <div className="register-container4">
          <span className="register-text5">Email              </span>
          <input 
            type="email" 
            value={email} 
            onChange={(e) => setEmail(e.target.value)}
            required
            className="input"
          />
        </div>
        <br />
        <div className="register-container5">
          <span className="register-text6">Phone Num   </span>
          <input 
            type="text" 
            value={phoneNumber} 
            onChange={(e) => setPhoneNumber(e.target.value)}
            required
            className="input"
          />
        </div>
        <br />
        <button className="register-button button" onClick={handleClickResigter}>Register</button>
      </div>
      <footer className="register-footer">
        <Link to="/" className="register-navlink1">
          <img
            alt="image"
            src="/playground_assets/ch1-200h.png"
            className="register-image2"
          />
        </Link>
        <span className="register-text7">
          © 2022 Team 2 Plat FSW26, All Rights Reserved.
        </span>
        <div className="register-icon-group1">
          <svg viewBox="0 0 950.8571428571428 1024" className="register-icon10">
            <path d="M925.714 233.143c-25.143 36.571-56.571 69.143-92.571 95.429 0.571 8 0.571 16 0.571 24 0 244-185.714 525.143-525.143 525.143-104.571 0-201.714-30.286-283.429-82.857 14.857 1.714 29.143 2.286 44.571 2.286 86.286 0 165.714-29.143 229.143-78.857-81.143-1.714-149.143-54.857-172.571-128 11.429 1.714 22.857 2.857 34.857 2.857 16.571 0 33.143-2.286 48.571-6.286-84.571-17.143-148-91.429-148-181.143v-2.286c24.571 13.714 53.143 22.286 83.429 23.429-49.714-33.143-82.286-89.714-82.286-153.714 0-34.286 9.143-65.714 25.143-93.143 90.857 112 227.429 185.143 380.571 193.143-2.857-13.714-4.571-28-4.571-42.286 0-101.714 82.286-184.571 184.571-184.571 53.143 0 101.143 22.286 134.857 58.286 41.714-8 81.714-23.429 117.143-44.571-13.714 42.857-42.857 78.857-81.143 101.714 37.143-4 73.143-14.286 106.286-28.571z"></path>
          </svg>
          <svg viewBox="0 0 877.7142857142857 1024" className="register-icon12">
            <path d="M585.143 512c0-80.571-65.714-146.286-146.286-146.286s-146.286 65.714-146.286 146.286 65.714 146.286 146.286 146.286 146.286-65.714 146.286-146.286zM664 512c0 124.571-100.571 225.143-225.143 225.143s-225.143-100.571-225.143-225.143 100.571-225.143 225.143-225.143 225.143 100.571 225.143 225.143zM725.714 277.714c0 29.143-23.429 52.571-52.571 52.571s-52.571-23.429-52.571-52.571 23.429-52.571 52.571-52.571 52.571 23.429 52.571 52.571zM438.857 152c-64 0-201.143-5.143-258.857 17.714-20 8-34.857 17.714-50.286 33.143s-25.143 30.286-33.143 50.286c-22.857 57.714-17.714 194.857-17.714 258.857s-5.143 201.143 17.714 258.857c8 20 17.714 34.857 33.143 50.286s30.286 25.143 50.286 33.143c57.714 22.857 194.857 17.714 258.857 17.714s201.143 5.143 258.857-17.714c20-8 34.857-17.714 50.286-33.143s25.143-30.286 33.143-50.286c22.857-57.714 17.714-194.857 17.714-258.857s5.143-201.143-17.714-258.857c-8-20-17.714-34.857-33.143-50.286s-30.286-25.143-50.286-33.143c-57.714-22.857-194.857-17.714-258.857-17.714zM877.714 512c0 60.571 0.571 120.571-2.857 181.143-3.429 70.286-19.429 132.571-70.857 184s-113.714 67.429-184 70.857c-60.571 3.429-120.571 2.857-181.143 2.857s-120.571 0.571-181.143-2.857c-70.286-3.429-132.571-19.429-184-70.857s-67.429-113.714-70.857-184c-3.429-60.571-2.857-120.571-2.857-181.143s-0.571-120.571 2.857-181.143c3.429-70.286 19.429-132.571 70.857-184s113.714-67.429 184-70.857c60.571-3.429 120.571-2.857 181.143-2.857s120.571-0.571 181.143 2.857c70.286 3.429 132.571 19.429 184 70.857s67.429 113.714 70.857 184c3.429 60.571 2.857 120.571 2.857 181.143z"></path>
          </svg>
          <svg viewBox="0 0 602.2582857142856 1024" className="register-icon14">
            <path d="M548 6.857v150.857h-89.714c-70.286 0-83.429 33.714-83.429 82.286v108h167.429l-22.286 169.143h-145.143v433.714h-174.857v-433.714h-145.714v-169.143h145.714v-124.571c0-144.571 88.571-223.429 217.714-223.429 61.714 0 114.857 4.571 130.286 6.857z"></path>
          </svg>
        </div>
      </footer>
    </div>
  )
}

export default Register

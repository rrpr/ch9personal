import React from 'react'
import { Link } from 'react-router-dom'

import PropTypes from 'prop-types'

import './listcard1.css'

const Listcard1 = (props) => {
  return (
    <div className={`listcard1-testimonial-card ${props.rootClassName} `}>
      <svg viewBox="0 0 950.8571428571428 1024" className="listcard1-icon">
        <path
          d="M438.857 182.857v402.286c0 161.143-131.429 292.571-292.571 292.571h-36.571c-20 0-36.571-16.571-36.571-36.571v-73.143c0-20 16.571-36.571 36.571-36.571h36.571c80.571 0 146.286-65.714 146.286-146.286v-18.286c0-30.286-24.571-54.857-54.857-54.857h-128c-60.571 0-109.714-49.143-109.714-109.714v-219.429c0-60.571 49.143-109.714 109.714-109.714h219.429c60.571 0 109.714 49.143 109.714 109.714zM950.857 182.857v402.286c0 161.143-131.429 292.571-292.571 292.571h-36.571c-20 0-36.571-16.571-36.571-36.571v-73.143c0-20 16.571-36.571 36.571-36.571h36.571c80.571 0 146.286-65.714 146.286-146.286v-18.286c0-30.286-24.571-54.857-54.857-54.857h-128c-60.571 0-109.714-49.143-109.714-109.714v-219.429c0-60.571 49.143-109.714 109.714-109.714h219.429c60.571 0 109.714 49.143 109.714 109.714z"
          className=""
        ></path>
      </svg>
      <div className="listcard1-testimonial">
        <img
          alt={props.image_alt}
          src={props.image_src}
          className="listcard1-image"
        />
        <span className="listcard1-text">{props.rpstitle}</span>
        <div className="listcard1-container">
          <Link to="/rpsdetail" className="listcard1-navlink button">
            {props.go}
          </Link>
        </div>
      </div>
    </div>
  )
}

Listcard1.defaultProps = {
  go: 'Go!',
  rpstitle: 'Rock Paper Scissors',
  image_src: '/playground_assets/istockphoto-1056840214-612x612-500h.jpg',
  rootClassName: '',
  image_alt: 'image',
}

Listcard1.propTypes = {
  go: PropTypes.string,
  rpstitle: PropTypes.string,
  image_src: PropTypes.string,
  rootClassName: PropTypes.string,
  image_alt: PropTypes.string,
}

export default Listcard1

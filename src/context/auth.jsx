import { createUserWithEmailAndPassword, onAuthStateChanged, signInWithEmailAndPassword, signOut } from "firebase/auth";
import { createContext, useCallback, useContext, useEffect, useState } from "react";
import { authInstance } from "../service/firebase";

const AuthContext = createContext(null);

export function useAuth() {
  return useContext(AuthContext);
}

export function AuthProvider({ children }) {
  const [user, setUser] = useState();
  const [accessToken, setAccessToken] = useState();

  const fetchUserDefinition = useCallback(() => {
    onAuthStateChanged(authInstance, async (user) => {
      if (user) {
        const accessToken = await user.getIdToken();
        setUser(user.toJSON());
        setAccessToken(accessToken);
      }
    });
  }, []);

  useEffect(() => {
    fetchUserDefinition();
  }, [fetchUserDefinition]);

  // BUAT LOGIN
  const signin = async (username, password) => {
    const result = await signInWithEmailAndPassword(authInstance, username, password);
    setUser(result.user.toJSON());
    const firebaseAccessToken = await result.user.getIdToken();
    setAccessToken(firebaseAccessToken);
  }

  // BUAT LOGOUT
  const signout = async () => {
    await signOut(authInstance);
    setUser(undefined);
    setAccessToken(undefined);
  }

  // BUAT REIGSTER
  const signup = async (email, password) => {
    if (!username || !password) return;
    const result = await createUserWithEmailAndPassword(authInstance, email, password);
    setUser(result.user.toJSON());
    const firebaseAccessToken = await result.user.getIdToken();
    setAccessToken(firebaseAccessToken);
  }

  return (
    <AuthContext.Provider value={{ user, accessToken, signin, signout, signup }}>{children}</AuthContext.Provider>
  );
}
import React from 'react'
import { Link } from 'react-router-dom'

import { Helmet } from 'react-helmet'

import './ttt.css'
import Game from "../components/Game/Game";

function tictactoe() {
  return (
    <div className="ttt">
      <Game />
    </div>
  );
}

export default tictactoe;

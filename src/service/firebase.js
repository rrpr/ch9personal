import { initializeApp } from "firebase/app";
import { getAuth } from 'firebase/auth';

const firebaseConfig = {
    apiKey: "AIzaSyAHfWvOpyRqaMYJXtHoA1IlOqfZCMGADis",
    authDomain: "challenge-chapter-9-c77ba.firebaseapp.com",
    projectId: "challenge-chapter-9-c77ba",
    storageBucket: "challenge-chapter-9-c77ba.appspot.com",
    messagingSenderId: "877455573077",
    appId: "1:877455573077:web:a177004e1fda34047354dc"
};

const app = initializeApp(firebaseConfig);

export const authInstance = getAuth(app);